﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts
{
    internal static class ConstantsCase
    {
        public enum CaseView
        {
            None = 0,
            Add,
            Remove,
            View
        };

        public enum MakeConsoleViewColorE
        {
            Main,
            AddContacts,
            RemoveContacts,
            ViewAllContacts,
            Default
        }

        public enum MakeElert
        {
            Danger,
            Warning,
            Success,
            Default
        }
    }
}
