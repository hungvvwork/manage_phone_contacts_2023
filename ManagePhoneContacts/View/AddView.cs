﻿using ManagePhoneContacts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.View
{
    internal class AddView : IView
    {
        private IUnitOfWork _UnitOfWork;
        public IUnitOfWork UnitOfWork => _UnitOfWork;

        public AddView(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }
        public void RunView()
        {
            DelegateHandleException DelegateHandleException = new DelegateHandleException(); // create handle exception class
            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.AddContacts);
            Console.Title = "___________________Add View___________________";

            DelegateHandleException.addConTactDelExCeption(_UnitOfWork.AddConTactDel); // handle exception


            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.Default);
        }
    }
}
