﻿using ManagePhoneContacts.Repository;
using ManagePhoneContacts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.View
{
    internal interface IView
    {
        void RunView();

        IUnitOfWork UnitOfWork { get; }
    }
}
