﻿using ManagePhoneContacts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.View
{
    internal class RemoveView : IView
    {
        private IUnitOfWork _UnitOfWork;
        public IUnitOfWork UnitOfWork => _UnitOfWork;

        public RemoveView(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }
        public void RunView()
        {
            DelegateHandleException delegateHandleException = new DelegateHandleException();
            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.RemoveContacts);
            Console.Title = "___________________Remove View___________________";
            delegateHandleException.removeContactsByIdDelExCeption(_UnitOfWork.RemoveContactsByIdDel);
            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.Default);

        }
    }
}
