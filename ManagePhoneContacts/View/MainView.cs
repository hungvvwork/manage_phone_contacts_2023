﻿
using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Factory;
using ManagePhoneContacts.Repository;
using ManagePhoneContacts.UnitOfWork;
using ManagePhoneContacts.Veryfy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.View
{
    internal class MainView : IView
    {
        private IView AddView { get; set; }
        private IView RemoveView { get; set; }
        private IView PrintAllView { get; set; }

        private IUnitOfWork _unitOfWork;
        public IUnitOfWork UnitOfWork => _unitOfWork;




        public MainView(IUnitOfWork unitOfWork, IView addview, IView removeview, IView printallview)
        {

            _unitOfWork = unitOfWork;
            AddView = addview;
            RemoveView = removeview;
            PrintAllView = printallview;
        }
        public void RunView()
        {

            bool breakApp = false;

            while (!breakApp)
            {
                Console.Title = "___________________Main View___________________";
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.Main);
                Menu();
                string isNumber;
                do
                {
                    isNumber = Console.ReadLine();
                } while (!_unitOfWork.Veryfy.IsNumber(isNumber));

                // Get Case
                ConstantsCase.CaseView CaseHandle = (ConstantsCase.CaseView)int.Parse(isNumber);

                switch (CaseHandle)
                {
                    case ConstantsCase.CaseView.Add: // Case Add , Add View Handle It
                        AddView.RunView();
                        break;
                    case ConstantsCase.CaseView.Remove: // Case Remove, Remove View Handle It
                        RemoveView.RunView();
                        break;
                    case ConstantsCase.CaseView.View:
                        PrintAllView.RunView();
                        break;
                    default:
                        break;
                }


                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.Default);

            }
        }


        public static void Menu()
        {
            Console.WriteLine("1. Add New Contacts");
            Console.WriteLine("2. Remove Contacts");
            Console.WriteLine("3. Get All Contacts");
        }
    }
}
