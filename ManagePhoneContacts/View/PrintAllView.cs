﻿using ManagePhoneContacts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.View
{
    internal class PrintAllView : IView
    {
        private IUnitOfWork _UnitOfWork;
        public IUnitOfWork UnitOfWork => _UnitOfWork;


        public PrintAllView(IUnitOfWork unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }
        public void RunView()
        {
            DelegateHandleException delegateHandleException = new DelegateHandleException(); // handle exception
            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.ViewAllContacts);
            Console.Title = "___________________PrintAll View___________________";

            
            foreach (var item in delegateHandleException.getAllContactsDelExCeption(_UnitOfWork.GetAllContactsDel)) //if null it return array lenght = 0
            {
                Console.WriteLine(item);
            };
            MakeColor.MakeConsoleViewColor(ConstantsCase.MakeConsoleViewColorE.Default);
        }
    }
}
