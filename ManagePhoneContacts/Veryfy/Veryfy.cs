﻿using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.Veryfy
{
    internal class Veryfy : IVeryfy
    {
        public bool IsExistsID(IContact[] contact, int i)
        {
            var result = false;
            if (contact is null)
            {
                return false;
            }
            foreach (var item in contact)
            {
                if (item.Id == i)
                {
                    result = true;
                    break;
                }
            };
            return result;
        }

        public bool IsNumber(string s)
        {
            int _;
            bool success = int.TryParse(s, out _);

            if (success)
            {
                return true;
            }
            return false;
        }
    }
}
