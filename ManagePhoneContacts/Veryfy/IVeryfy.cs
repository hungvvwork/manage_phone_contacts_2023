﻿using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.Veryfy
{
    internal interface IVeryfy
    {

        /// <summary>
        /// is number
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        bool IsNumber(string s);
        /// <summary>
        /// is exists, is s exists in contact?
        /// </summary>
        /// <param name="s"></param>
        /// <param name="contact"></param>
        /// <returns></returns>
        bool IsExistsID(IContact[] contact, int s);

    }
}
