﻿using ManagePhoneContacts.DbPhoneContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.Repository
{

    /// <summary>
    /// Create With IDbPhoneContacts
    /// </summary>
    internal class RepositoryContacts : IRepositoryContacts
    {
        private IDbPhoneContacts _dbPhoneContacts;
        public IDbPhoneContacts dbPhoneContacts => _dbPhoneContacts;


        public RepositoryContacts(IDbPhoneContacts dbPhoneContacts)
        {
            _dbPhoneContacts = dbPhoneContacts;
        }


        /// <summary>
        /// Add Contact 
        /// </summary>
        /// <param name="contact"></param>
        public void AddContacts(IContact contact)
        {
            _dbPhoneContacts.AddContacts(contact);
        }

        /// <summary>
        /// Delete Contacts
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void RemoveContactsById(int id)
        {
            _dbPhoneContacts.RemoveContactById(id);
        }

        /// <summary>
        /// Get All Contacts
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IContact[] GetAllContacts()
        {
            return _dbPhoneContacts.GetAllContacts();
        }

        /// <summary>
        /// Get Contact By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IContact GetContactsById(int id)
        {
            return _dbPhoneContacts.GetContactsById(id);
        }
    }
}
