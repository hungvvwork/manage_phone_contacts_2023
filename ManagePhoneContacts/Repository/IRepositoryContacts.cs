﻿using ManagePhoneContacts.DbPhoneContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.Repository
{
    internal interface IRepositoryContacts
    {
        IDbPhoneContacts dbPhoneContacts { get; }

        //Delete Contact
        void RemoveContactsById(int id);



        // Get Contacts
        IContact[] GetAllContacts();
        IContact GetContactsById(int id);



        //Add Contacts

        void AddContacts(IContact contact);
    }
}
