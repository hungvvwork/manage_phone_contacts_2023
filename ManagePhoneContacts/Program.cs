﻿using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Factory;
using ManagePhoneContacts.Repository;
using ManagePhoneContacts.UnitOfWork;
using ManagePhoneContacts.Veryfy;
using ManagePhoneContacts.View;

class Program
{

    public static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        // data base, resourse data
        IDbPhoneContacts dbPhoneContacts = new DbPhoneContacts();


        // factory. for create contacts
        IFactory factory = new Factory();


        //repository, seprate get data
        IRepositoryContacts repository = new RepositoryContacts(dbPhoneContacts);

        //veryfy input data
        IVeryfy veryfy = new Veryfy();


        //unit of work

        IUnitOfWork unitOfWork = new UnitOfWork(repository, veryfy, factory);


        // create kind of view
        IView addView = new AddView(unitOfWork); // handle add new contact
        IView removeView = new RemoveView(unitOfWork); // handle remove contact by id
        IView printView = new PrintAllView(unitOfWork); // handle print all contact ro console



        // Main, retrive add view to create main view
        IView MainView = new MainView(unitOfWork, addView, removeView, printView);


        MainView.RunView();

    }



}