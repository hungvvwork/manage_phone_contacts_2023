﻿using ManagePhoneContacts.DbPhoneContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.Factory
{
    internal interface IFactory
    {
        IContact CreateContact();
    }
}
