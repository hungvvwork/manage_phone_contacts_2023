﻿using ManagePhoneContacts.DbPhoneContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ManagePhoneContacts.ConstantsCase;

namespace ManagePhoneContacts.Factory
{
    internal class Factory : IFactory
    {
        public IContact CreateContact()
        {
            IContact contact = new Contact();
            contact.Id = CreateId();
            contact.Name = CreateName();
            contact.Email= CreateEmail();
            contact.PhoneNumber= CreatePhone();
            return contact;
        }

        /// <summary>
        /// Create Id Contacts
        /// </summary>
        /// <returns></returns>
        public int CreateId()
        {
            int result;
            Console.WriteLine("Vui Lòng Nhập ID");
            Console.Write("Your ID :");

            while (!int.TryParse(Console.ReadLine(), out result))
            {
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);
                Console.WriteLine("Vui Lòng Nhập ID Là Số");
                Console.Write("Your ID :");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);
            }

            return result;
        }

        /// <summary>
        /// Create Name
        /// </summary>
        /// <returns></returns>
        public string CreateName()
        {
            Console.WriteLine("Vui Lòng Nhập Name");
            Console.Write("Your Name :");
            string name = Console.ReadLine();

            while (string.IsNullOrEmpty(name))
            {
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Danger);
                Console.WriteLine("Vui Lòng Nhập Name, Không Được Bỏ Trống");
                Console.Write("Your Name :");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);
                name = Console.ReadLine();
            }
            return name;
        }

        /// <summary>
        /// Create Email
        /// </summary>
        /// <returns></returns>
        public string CreateEmail()
        {
            Console.WriteLine("Vui Lòng Nhập Email");
            Console.Write("Your Email :");
            string Email = Console.ReadLine();
            while (string.IsNullOrEmpty(Email))
            {

                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);
                Console.WriteLine("Vui Lòng Nhập Email, Trường Email Không Được Bỏ Trống");
                Console.Write("Your Email :");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);

                Email = Console.ReadLine();
            }
            return Email;
        }
        /// <summary>
        /// Create Number
        /// </summary>
        /// <returns></returns>
        public int CreatePhone()
        {
            int YourNumber;
            Console.WriteLine("Vui Lòng Nhập SĐT");
            Console.Write("Your Number :");
            bool RequireNumber = !int.TryParse(Console.ReadLine(), out YourNumber);
            while (!RequireNumber)
            {
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);

                Console.WriteLine("Vui Lòng Nhập SĐT, Là Số");
                Console.Write("Your Number :");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);
                RequireNumber = int.TryParse(Console.ReadLine(), out YourNumber);
            }

            return YourNumber;
        }


    }
}
