﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ManagePhoneContacts.ConstantsCase;

namespace ManagePhoneContacts
{
    internal static class MakeColor
    {
        public static void MakeConsoleViewColor(MakeConsoleViewColorE contactsCaseColor)
        {
            switch (contactsCaseColor)
            {
                case ConstantsCase.MakeConsoleViewColorE.AddContacts:
                    Console.ResetColor();
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case ConstantsCase.MakeConsoleViewColorE.RemoveContacts:
                    Console.ResetColor();
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case ConstantsCase.MakeConsoleViewColorE.ViewAllContacts:
                    Console.ResetColor();
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case ConstantsCase.MakeConsoleViewColorE.Main:
                    Console.ResetColor();
                    Console.WriteLine("Press any key to continues");
                    Console.ReadKey();
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case ConstantsCase.MakeConsoleViewColorE.Default:
                    Console.ResetColor();
                    break;
            }
        }

        public static void MakeConsoleViewColor(MakeElert makeElert)
        {
            switch (makeElert)
            {
                case ConstantsCase.MakeElert.Success:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case ConstantsCase.MakeElert.Danger:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case ConstantsCase.MakeElert.Warning:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case ConstantsCase.MakeElert.Default:
                    Console.ResetColor();
                    break;
            }
        }

    }
}
