﻿using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Factory;
using ManagePhoneContacts.Repository;
using ManagePhoneContacts.Veryfy;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.UnitOfWork
{


    internal class UnitOfWork : IUnitOfWork
    {
        IRepositoryContacts _repositoryContacts;

        IVeryfy _veryfy;

        IFactory _factory;
        public IRepositoryContacts RepositoryContacts => _repositoryContacts;

        public IVeryfy Veryfy => _veryfy;

        public IFactory Factory => _factory;

        //PROTECTED DATA
        private AddContactDel _AddConTactDel;
        private RemoveContactsByIdDel _RemoveContactsByIdDel;
        private GetAllContactsDel _GetAllContactsDel;


        //SHOW => END THEN HANDLE EXCEPTION IN VIEW, AND HIDING DATA;
        public AddContactDel AddConTactDel => _AddConTactDel;

        public RemoveContactsByIdDel RemoveContactsByIdDel => _RemoveContactsByIdDel;

        public GetAllContactsDel GetAllContactsDel => _GetAllContactsDel;

        // CREATE DEL IN  CONTRUCTOR
        public UnitOfWork(IRepositoryContacts repositoryContacts, IVeryfy veryfy, IFactory factory)
        {
            _repositoryContacts = repositoryContacts;
            _veryfy = veryfy;
            _factory = factory;
            _AddConTactDel = new AddContactDel(AddContacts);
            _RemoveContactsByIdDel = new RemoveContactsByIdDel(RemoveContactsById);
            _GetAllContactsDel = new GetAllContactsDel(GetAllContacts);
        }

        /// <summary>
        /// Add Contacts
        /// </summary>
        /// <returns></returns>
        private IContact AddContacts()
        {
            IContact newContact = _factory.CreateContact(); //Factory
            if (!_veryfy.IsExistsID(_repositoryContacts.GetAllContacts(), newContact.Id))
            {
                _repositoryContacts.AddContacts(newContact); // add contact
            }
            else
            {
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);
                Console.WriteLine("ID Already Exists");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);
            }


            return newContact;
        }
        /// <summary>
        /// Remove Contact by id
        /// </summary>
        private void RemoveContactsById()
        {
            Console.Write("Please Add Id : ");
            string inputId = Console.ReadLine();

            if (!_veryfy.IsNumber(inputId))
            {
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);

                Console.WriteLine("It Not Number");
                MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);

            }
            else
            {

                if (_veryfy.IsExistsID(_repositoryContacts.GetAllContacts(), int.Parse(inputId))) //update and veryfy 
                {
                    _repositoryContacts.RemoveContactsById(int.Parse(inputId));
                    MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Success);
                    Console.WriteLine("Success");
                    MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);

                }
                else
                {
                    MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Warning);
                    Console.WriteLine("ID Not Exists");
                    MakeColor.MakeConsoleViewColor(ConstantsCase.MakeElert.Default);

                };
            }
        }
        /// <summary>
        /// Get All Contacts
        /// </summary>
        /// <returns></returns>
        private IContact[] GetAllContacts()
        {
            return _repositoryContacts.GetAllContacts();
        }


        public IContact GetContactsById()
        {
            throw new NotImplementedException();
        }
    }
}
