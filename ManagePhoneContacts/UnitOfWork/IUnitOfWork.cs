﻿using ManagePhoneContacts.DbPhoneContacts;
using ManagePhoneContacts.Factory;
using ManagePhoneContacts.Repository;
using ManagePhoneContacts.Veryfy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ManagePhoneContacts.UnitOfWork
{
    internal interface IUnitOfWork
    {
        IRepositoryContacts RepositoryContacts { get; }

        IVeryfy Veryfy { get; }

        IFactory Factory { get; }


        AddContactDel AddConTactDel { get; }

        RemoveContactsByIdDel RemoveContactsByIdDel { get; }

        GetAllContactsDel GetAllContactsDel { get; }

        IContact GetContactsById();
    }
}
