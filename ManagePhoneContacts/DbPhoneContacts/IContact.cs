﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.DbPhoneContacts
{
    public interface IContact
    {

        int Id { get; set; }

        string Name { get; set; }

        string Email { get; set; }

        int PhoneNumber { get; set; }


    }
}
