﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts.DbPhoneContacts
{
    internal interface IDbPhoneContacts
    {
        IContact[] Contacts { get; }

        //Delete Contact
        bool RemoveContactById(int id);



        // Get Contacts
        IContact[] GetAllContacts();
        IContact GetContactsById(int id);



        //Add Contacts

        bool AddContacts(IContact contact);
    }
}
