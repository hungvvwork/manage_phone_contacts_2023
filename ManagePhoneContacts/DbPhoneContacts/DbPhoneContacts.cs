﻿using System;
using System.IO;
using System.Text.Json;

namespace ManagePhoneContacts.DbPhoneContacts
{
    internal class DbPhoneContacts : IDbPhoneContacts
    {
        private IContact[] _contacts;
        public IContact[] Contacts { get => _contacts; }
        public string filePath = "MyData3.txt";

        public DbPhoneContacts()
        {
            InitializeContacts();
        }


        /// <summary>
        /// Create File Contacts
        /// </summary>
        private void InitializeContacts()
        {
            // Is file exists?
            if (File.Exists(filePath))
            {
                ReaderData();
            }
            else
            {
                WriterData(); // it create if file not exists => _contacts null
            }
        }


        /// <summary>
        /// Add Contacts
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public bool AddContacts(IContact contact)
        {
            //Update 
            ReaderData();
            // Add new contact to array
            Array.Resize(ref _contacts, Contacts.Length + 1);
            _contacts[Contacts.Length - 1] = contact;

            WriterData();
            return true;
        }

        /// <summary>
        /// Delete Contacts by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool RemoveContactById(int id)
        {
            //Update 
            ReaderData();
            // create new array, and asign value to it
            IContact[] _newArrayContacts = Array.FindAll(_contacts, item => item != null && item.Id != id); // if != null and !id => get

            _contacts = _newArrayContacts;

            // use using key
            using (StreamWriter writer = new StreamWriter(filePath, false))
            {
                // write data to file
                string jsonContacts = JsonSerializer.Serialize(_contacts);
                writer.Write(jsonContacts);
            }

            return true;
        }

        /// <summary>
        /// Return All Contacts
        /// </summary>
        /// <returns></returns>
        public IContact[] GetAllContacts()
        {
            //Update 
            ReaderData();
            return _contacts;
        }

        /// <summary>
        /// Return Contact By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IContact GetContactsById(int id)
        {
            //Update 
            ReaderData();

            IContact contact = Array.Find(_contacts, contacts => contacts.Id == id);

            return contact;
        }


        /// <summary>
        /// Read file and asign it to _contacts, if empty create new array with lenght draw 0
        /// </summary>
        private void ReaderData()
        {
            // use Using key to release resource after use it
            using (StreamReader reader = new StreamReader(filePath))
            {
                // read content of file
                string jsonData = reader.ReadToEnd();

                // cover to json
                if (!string.IsNullOrEmpty(jsonData))
                {
                    _contacts = JsonSerializer.Deserialize<Contact[]>(jsonData);
                }
                else
                {
                    // if empty create new array Icontacts with lengt = 0
                    _contacts = new IContact[0];
                }
            }
        }

        /// <summary>
        /// write data to file
        /// </summary>
        private void WriterData()
        {
            using (StreamWriter writer = new StreamWriter(filePath, append: false))
            {
                if (_contacts is null)
                {
                    _contacts = new IContact[0]; //handle fist call init _contacts not null
                }
                string textJson = JsonSerializer.Serialize(_contacts);

                writer.Write(textJson);
            }
        }
    }
}
