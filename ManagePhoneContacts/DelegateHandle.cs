﻿using ManagePhoneContacts.DbPhoneContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePhoneContacts
{
    // USE FOR CALL BACK HANDLE EXCEPTION
    public delegate IContact AddContactDel();
    public delegate void RemoveContactsByIdDel();
    public delegate IContact[] GetAllContactsDel();

    //handle exception add new contact
    public delegate void AddConTactDelExCeption(AddContactDel addConTactDel);
    public delegate void RemoveContactsByIdDelExCeption(RemoveContactsByIdDel removeContactsByIdDel);
    public delegate IContact[] GetAllContactsDelExCeption(GetAllContactsDel getAllContactsDel);


    public class DelegateHandleException
    {
        private AddConTactDelExCeption _addConTactDelExCeption;
        private RemoveContactsByIdDelExCeption _removeContactsByIdDelExCeption;
        private GetAllContactsDelExCeption _getAllContactsDelExCeption;

        /// <summary>
        /// handle add new contact
        /// </summary>
        public AddConTactDelExCeption addConTactDelExCeption => _addConTactDelExCeption;

        /// <summary>
        /// handle remove contact
        /// </summary>
        /// <param name="removeContactsByIdDel"></param>

        public RemoveContactsByIdDelExCeption removeContactsByIdDelExCeption => _removeContactsByIdDelExCeption;
        /// <summary>
        /// handle print all
        /// </summary>
        public GetAllContactsDelExCeption getAllContactsDelExCeption => _getAllContactsDelExCeption;

        public DelegateHandleException()
        {
            _addConTactDelExCeption = new AddConTactDelExCeption(HandleExceptionAddContactdel);
            _removeContactsByIdDelExCeption = new RemoveContactsByIdDelExCeption(HandleExceptionRemoveContactsByIdDel);
            _getAllContactsDelExCeption = new GetAllContactsDelExCeption(HandleExceptionGetAllContactsDel);
        }

        /// <summary>
        /// handle add new contact
        /// </summary>
        /// <param name="addConTactDel"></param>
        private void HandleExceptionAddContactdel(AddContactDel addConTactDel)
        {
            try
            {
                addConTactDel();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
        }
        /// <summary>
        /// handle remove contact
        /// </summary>
        /// <param name="removeContactsByIdDel"></param>
        private void HandleExceptionRemoveContactsByIdDel(RemoveContactsByIdDel removeContactsByIdDel)
        {
            try
            {
                removeContactsByIdDel();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
        }
        /// <summary>
        /// handle print all
        /// </summary>
        /// <param name="getAllContactsDel"></param>

        private IContact[] HandleExceptionGetAllContactsDel(GetAllContactsDel getAllContactsDel)
        {
            IContact[] result = null;
            try
            {
                result = getAllContactsDel();
            }
            catch (Exception Ex)
            {
                result = new IContact[0];
                Console.WriteLine(Ex.Message);
            }

            return result;
        }

    }
}
